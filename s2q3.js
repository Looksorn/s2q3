const puppeteer = require('puppeteer');

async function getFundsData () {

    const browser = await puppeteer.launch({ headless: false })
    const page = await browser.newPage()
   
    await page.setViewport({ width: 1280, height: 800 })
    await page.goto('https://codequiz.azurewebsites.net/')
   
    const navigationPromise = page.waitForNavigation()
   
    await page.waitForSelector('input[type="button"]')
    await page.click('input[type="button"]')
   
    await navigationPromise
    const result = await page.evaluate(() => {
        let headers = [];
        document.querySelectorAll('th').forEach(element => headers.push(element.innerHTML));

        let data = [];
        document.querySelectorAll('td').forEach(element => data.push(element.innerHTML));
        
        return {headers, data};
    });

    await navigationPromise
    await page.waitForSelector('input[type="button"]')
    await page.click('input[type="button"]')
   
    await navigationPromise
   
    await browser.close()

    return result;
}

async function removeSpaceBar(arr) {
    return await arr.map(function (x) { return x.replace(' ', ''); });
}

async function getNavByFundName (headers, data, fundName) {
    const navPosition = headers.indexOf('Nav');
    const fundPosition = data.indexOf(fundName);

    if (navPosition === -1 || fundPosition === -1) {
        return ;
    }
    return data[fundPosition + navPosition];
}

(async () => {

    const fundName = process.argv.slice(2)[0];
    
    let {headers, data} = await getFundsData();

    headers = await removeSpaceBar(headers);
    data = await removeSpaceBar(data);

    const result = await getNavByFundName(headers, data, fundName);
    if (result) {
        console.log(result);
    }

})()